//
//  MyNbaCompanionApp.swift
//  MyNbaCompanion
//
//  Created by Azzedine AIT ELHAJ on 18/11/2022.
//

import SwiftUI

@main
struct MyNbaCompanionApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
